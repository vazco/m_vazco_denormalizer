A = new Mongo.Collection('a');
B = new Mongo.Collection('b');
C = new Mongo.Collection('c');

if (Meteor.isServer) {
    Meteor.methods({
        clearDocs: function () {
            A.remove({});
            B.remove({});
            C.remove({});
            return true;
        }
    });
}

B.denormalize({
    key: 'c_id',
    collection: C,
    fields: {
        title: 'c_title',
        title2: 'c_title2',
        noValue: 'noValue'
    }
});

A.denormalize({
    key: 'b_id',
    collection: B,
    fields: {
        title: 'b_title',
        c_title: 'c_title',
        noValue: 'noValue'
    }
});

A.denormalize({
    key: 'c_id',
    collection: C,
    fields: {
        title: 'c_title',
        title2: 'c_title2',
        noValue: 'noValue'
    }
});

Tinytest.addAsync('Denormalize A-B-C on insert and update', function (test, next) {
    Meteor.call('clearDocs', function () {

        C.insert({title: 'C doc'}, function (err, c_id) {

            B.insert({title: 'B doc', c_id: c_id}, function (err, b_id) {

                A.insert({b_id: b_id}, function (err, a_id) {

                    test.equal(A.findOne(a_id),
                        {_id: a_id, b_title: 'B doc', c_title: 'C doc', b_id: b_id},
                        'on insert');

                    B.update(b_id, {$set: {title: 'B doc change'}}, function () {

                        test.equal(A.findOne(a_id),
                            {_id: a_id, b_title: 'B doc change', c_title: 'C doc', b_id: b_id},
                            'on update');

                        C.update(c_id, {$set: {title: 'C doc change'}}, function () {

                            test.equal(A.findOne(a_id),
                                {_id: a_id, b_title: 'B doc change', c_title: 'C doc change', b_id: b_id},
                                'A to C');

                            C.insert({title: 'C2 doc'}, function (err, c2_id) {

                                B.update(b_id, {$set: {c_id: c2_id}}, function () {

                                    test.equal(B.findOne(b_id),
                                        {
                                            _id: b_id,
                                            title: 'B doc change',
                                            c_title: 'C2 doc',
                                            c_id: c2_id
                                        },
                                        'change B to C relation on B');

                                    test.equal(A.findOne(a_id),
                                        {
                                            _id: a_id,
                                            b_title: 'B doc change',
                                            c_title: 'C2 doc',
                                            b_id: b_id
                                        },
                                        'change B to C relation on A');

                                    B.insert({title: 'B2 doc'}, function (err, b2_id) {

                                        A.update(a_id, {$set: {b_id: b2_id}}, function () {

                                            test.equal(A.findOne(a_id),
                                                {
                                                    _id: a_id,
                                                    b_title: 'B2 doc',
                                                    b_id: b2_id
                                                },
                                                'change A to B relation on A (should no longer have C fields)');

                                            next();
                                        });
                                    });
                                });
                            });
                        });
                    });
                })
            });
        });
    })
});

Tinytest.addAsync('Denormalize A-B and A-C on update', function (test, next) {
    Meteor.call('clearDocs', function () {

        C.insert({title: 'C doc'}, function (err, c_id) {

            B.insert({title: 'B doc'}, function (err, b_id) {

                A.insert({title: 'A doc'}, function (err, a_id) {

                    test.equal(A.findOne(a_id),
                        {_id: a_id, title: 'A doc'},
                        'there should be no relations so far on A');

                    A.update(a_id, {$set: {b_id: b_id, c_id: c_id}}, function () {

                        test.equal(A.findOne(a_id),
                            {
                                _id: a_id,
                                title: 'A doc',
                                b_id: b_id,
                                b_title: 'B doc',
                                c_id: c_id,
                                c_title: 'C doc'
                            },
                            'there should be A to B and A to C realtion on A');

                        C.update(c_id, {$set: {title2: 'C title2'}}, function () {

                            test.equal(A.findOne(a_id),
                                {
                                    _id: a_id,
                                    title: 'A doc',
                                    b_id: b_id,
                                    b_title: 'B doc',
                                    c_id: c_id,
                                    c_title: 'C doc',
                                    c_title2: 'C title2'
                                },
                                'there should be another field from C on A');

                            C.update(c_id, {$unset: {title: '', title2: ''}}, function () {

                                test.equal(A.findOne(a_id),
                                    {
                                        _id: a_id,
                                        title: 'A doc',
                                        b_id: b_id,
                                        b_title: 'B doc',
                                        c_id: c_id
                                    },
                                    'c_title and c_title2 should disappear on A');
                                next();
                            });
                        });
                    });
                });
            });
        });
    });
});
