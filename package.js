Package.describe({
    summary: 'Vazco Denormalizer',
    name: 'vazco:denormalizer'
});

Package.on_use(function (api) {
    api.use([
        'underscore',
        'mongo',
        'matb33:collection-hooks'
    ]);

    api.add_files([
        'denormalizer.js'
    ]);
});

Package.on_test(function (api) {
    api.use([
        'vazco:denormalizer',
        'tinytest',
        'mongo',
        'insecure',
        'autopublish'
    ]);

    api.add_files([
        'tests.js'
    ]);
});