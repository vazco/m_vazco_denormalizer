'use strict';
/*
 id <sting> - id of document
 fields <object> - fields to get from foreign document { foreignField: 'localField' }
 collection <object> - where to find (collection object)

 if document exist it will be passed to extractSetAndUnset function.
 */

var getForeignFields = function (id, fields, collection) {
    var otherDoc = collection.findOne(id);

    if (otherDoc) {
        return extractSetAndUnset(otherDoc, fields);
    }
};

/*
 takes document and list of fields and gives you array of 2 object that you can pass to mongo $set and $unset.
 if fields exists (has value) it is put in set object. if field is undefined it will be put in unset object

 returns [ $set <object>, $unset <object> ]
 */
var extractSetAndUnset = function (doc, fields, previousDoc) {
    var toSet = {}, toUnset = {};
    _(fields).each(function (newKey, oldKey) {
        // iterates through defined fields and pass values to either
        // toSet or (if field is undefined) to toUnset object.
        var newValue = doc[oldKey];
        var oldValue = previousDoc && previousDoc[oldKey];

        if (!_.isUndefined(newValue)) {
            if (!oldValue || !_.isEqual(newValue, oldValue)) {
                toSet[newKey] = newValue;
            }
        } else {
            toUnset[newKey] = '';
        }
    });
    return [toSet, toUnset];
};

Mongo.Collection.prototype.denormalize = function (params) {
    params = params || {};
    var self = this;

    // check settings.json for migrate flag
    if(Meteor.isServer && Meteor.settings && Meteor.settings.public && Meteor.settings.public.denormalizerMigrate){

        // migrate DB
        this._denormalizerMigrate(params);
    }

    // when document with relation in inserted.
    this.before.insert(function (userId, doc) {
        var foreignFields, id;

        // key field should contains id to related document
        id = lookup(params.key, doc);

        if (id) {
            // extend document by fields from denormalized document.
            foreignFields = getForeignFields(id, params.fields, params.collection);
            if (foreignFields) {
                _(doc).extend(foreignFields[0]);
            }
        }
    });

    // when document changes relation.
    this.after.update(function (userId, doc, fieldNames) {
        var id = lookup(params.key, doc), foreignFields;
        var mod;

        // check if relation has changed (by checking key field)
        if (_(fieldNames).contains(topFieldPath(params.key))) {

            // check if document is still in relationship
            if (id) {
                // when relation exists
                foreignFields = getForeignFields(id, params.fields, params.collection);
                if (foreignFields && foreignFields.length) {
                    mod = {};
                    if(_.isObject(foreignFields[0]) && !_.isEmpty(foreignFields[0])){
                        mod.$set = foreignFields[0];
                    }
                    //CR: Empty $set or $unset throws exception!
                    if(_.isObject(foreignFields[1]) && !_.isEmpty(foreignFields[1])){
                        mod.$unset = foreignFields[1];
                    }
                    if(!_.isEmpty(mod)){
                        self.update(doc._id, mod);
                    }
                }
            } else {
                // document is no longer in relationship so we can
                // remove previous denormalized data
                mod ={ $unset: _.invert(params.fields)};
                if(_.isObject(mod.$unset) && !_.isEmpty(mod.$unset)){
                    self.update(doc._id, {$unset: _.invert(params.fields)});
                }
            }
        }
    });

    if (Meteor.isServer) {
        // client code can only update by _id so this can work only on server.

        // when denormalized document is changed
        params.collection.after.update(function (userId, doc, fieldNames) {
            var affectedFields, extractedFields, query = {}, mod;

            // intersection between fields changed by update and denormalized fields.
            affectedFields = _(params.fields).pick(fieldNames);

            if (_.size(affectedFields) > 0) {
                // when at least one of denormalized fields was updated

                // extract changes
                extractedFields = extractSetAndUnset(doc, affectedFields, this.previous);

                // find documents in relation to this document and update them.
                query[params.key] = doc._id;

                mod = {};
                if (!_.isEmpty(extractedFields[0])) {
                    mod.$set = extractedFields[0];
                }
                if (!_.isEmpty(extractedFields[1])) {
                    mod.$unset = extractedFields[1];
                }
                if (!_.isEmpty(mod)) {
                    self.update(query, mod, {multi: true});
                }
            }
        });
    }
};

// method for migrating from normalized to denormalized db.
// takes same arguments as denormalize method.
Mongo.Collection.prototype._denormalizerMigrate = function (params) {
    params = params || {};
    var self = this, query = {};

    query[params.key] = {$exists: true};

    this.find(query).forEach(function (doc) {
        var id = lookup(params.key, doc),
            foreignFields = getForeignFields(id, params.fields, params.collection),
            mod;

        if (foreignFields) {
            mod = {};
            if (!_.isEmpty(foreignFields[0])) {
                mod.$set = foreignFields[0];
            }
            if (!_.isEmpty(foreignFields[1])) {
                mod.$unset = foreignFields[1];
            }
            if (!_.isEmpty(mod)) {
                self.update(doc._id, mod);
            }
        }
    });
};

var lookup = function (path, object) {
    var arr = path.split('.');
    while (arr.length && (object = object[arr.shift()]));
    return object;
};

var topFieldPath = function (path) {
    return path.split('.')[0];
};
