#Denormalizer

##API

    Collection.denormalize({
       key: 'another_id', //field where you store id to foreign document
       collection: AnotherCollection, //collection where you store foreign document
       fields: {
           foreignFieldName: 'willBeDenormalizedToThisField',
           antotherForeignField: 'localField'
       }
    });

## Examples

    Posts.denormalize({
		key: 'author_id',
		collection: Meteor.users,
		fields: {
			profile: 'authorProfile'
		}
	});

Now when you insert or update documents in Posts collection they will automaticly includes fields from Meteor.users collection. Any change in docs form Meteor.users collection will be reflected in Posts documents.

    console.log(Meteor.users.findOne('v3H2W2D3YCKqziEAi'));
    /*
	{
		_id: 'v3H2W2D3YCKqziEAi',
		profile: {
			name: 'John Smith'
   		}
	}
	*/

	Posts.insert({ title: 'Test post', author_id: 'v3H2W2D3YCKqziEAi'});
	// or
	Posts.update('u9MxEcmNTuTKBW9Bp', { $set: { author_id: 'v3H2W2D3YCKqziEAi'}});

    console.log(Posts.findOne({title: 'Test post'}));
    /*
	{
		_id: 'u9MxEcmNTuTKBW9Bp',
		title: 'Test post',
		authorProfile: {
			name: 'John Smith'
   		}
	}
	*/

	Meteor.users.update('v3H2W2D3YCKqziEAi', {$set: {profile: {name: 'Andrzej'}});
	console.log(Posts.findOne({title: 'Test post'}));
	/*
    	{
    		_id: 'v3H2W2D3YCKqziEAi',
    		profile: {
    			name: 'Andrzej'
       		}
    	}
    */

##Migration

For migrating from normalized DB run meteor this settings.json:
```
{
  "public": {
    "denormalizerMigrate": true
  }
}
```

It is only required once.

##Limitations

* No support for circular realtions of any kind.
* Denormalization work only with top level fields and it won't work with 'field.subfield' notation.
* No support for one to many relations.
* No action is taken when denormalized document is removed.
* 
